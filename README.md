# urp-world
URP World is a project developed by Alexandre Kikuchi (Tech Evangelist) to demonstrate Unity features such as URP, Shader Graph, Terrain Tools and more.
The project is available on GCL and the goal is to be used for different talks and be always up to date.
It will have a few different games, but the first one is a mix of strategy with tower defense.
Check each game's Briefing in the root of the game's folder.
